<?php echo form_open('/transfer/create', 'class="form"'); ?>
<?php echo validation_errors(); ?>
    <div class="form-group">
        <?php echo form_label($this->transferModel->attributeLabel()['sender_id']);?>
        <?php echo form_dropdown('sender_id', $this->accountModel->getAll(), array(), 'class="form-control"');?>
    </div>
    <div class="form-group">
        <?php echo form_label($this->transferModel->attributeLabel()['recipient_id']);?>
        <?php echo form_dropdown('recipient_id', $this->accountModel->getAll(), array(), 'class="form-control"');?>
    </div>

    <div class="form-group">
        <?php echo form_label($this->transferModel->attributeLabel()['summ']);?>
        <?php echo form_input(array(
            'name' => 'summ',
            'class' => 'form-control',
            'placeholder' => 'Cумма перевода',
        ));?>
    </div>
<?php echo form_submit('submit', 'Отправить'); ?>
<?php echo form_close(); ?>