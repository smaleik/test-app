<?php echo form_open('/client/create','class="form"');?>
<?php echo validation_errors();?>
<div class="form-group">

    <?php echo form_label('client');?>
    <?php echo form_input(array(
        'name' => 'client',
        'maxlength' => '255',
        'size' => '100',
        'class'=>'form-control',
        'placeholder'=>'Имя',
    ));?>
</div>
<div class="form-group">
    <?php echo form_label('serial');?>
    <?php echo form_input(array(
        'name' => 'serial',
        'maxlength' => '255',
        'size' => '100',
        'class'=>'form-control',
        'placeholder'=>'Номер счета',
    ));?>
</div>
<div class="form-group">
    <?php echo form_label('balance');?>
    <?php echo form_input(array(
        'name' => 'balance',
        'maxlength' => '255',
        'size' => '100',
        'class'=>'form-control',
        'placeholder'=>'Баланс',
    ));?>
</div>
<?php echo form_submit('submit','Отправить');?>
<?php echo form_close();?>