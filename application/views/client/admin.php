<table class="table">
    <thead>
    <tr>
        <th>id</th>
        <th>ФИО</th>
        <th>Номер счета</th>
        <th>Баланс</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($accounts->result() as $account) : ?>
    <tr>
        <td><?php echo $account->id?></td>
        <td><?php echo $account->client?></td>
        <td><a href="<?php echo site_url('/client/detail/'.$account->serial)?>"><?php echo $account->serial?></a></td>
        <td><?php echo $account->balance?></td>
    </tr>
    <?php endforeach;?>

    </tbody>
</table>