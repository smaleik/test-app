<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cezar
 * Date: 01.10.14
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */

class AccountDetailModel  extends MY_Model {
    public $id;
    public $account_id;
    public $serial;
    public $in;
    public $out;

    /**
     * Название таблицы с которой работает модель
     * @return string
     */
    public function tableName(){
        return 'account_detail';
    }

    /**
     * Правила для аттрибутов модели
     * @return array
     */
    public function rules(){
       return array(

        );
    }

    /**
     * Подписи для атрибутов модели
     * @return array
     */
    public function attributeLabel(){
        return array(

        );
    }
}