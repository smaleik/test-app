<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cezar
 * Date: 01.10.14
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */

class TransferModel  extends MY_Model {

    public $sender_id;
    public $recipient_id;
    public $summ;
    public $commision;

    const COMMISSION_STATE = 0.99; // коммисия для переводов
    const SYSTEM_SERIAL = 0 ; // счет системного счета на который будет начисляться комиссия
    /**
     * Название таблицы с которой работает модель
     * @return string
     */
    public function tableName(){
        return 'transfer';
    }

    /**
     * Правила для аттрибутов модели
     * @return array
     */
    public function rules(){
       return array(
            'sender_id'=>'required|callback_check_balance_sender['.$this->input->post('summ').']',
            'recipient_id'=>'required|callback_check_sender_recipient['.$this->input->post('sender_id').']',
            'summ'=>'required',
        );
    }

    /**
     * Подписи для атрибутов модели
     * @return array
     */
    public function attributeLabel(){
        return array(
          'sender_id'=>'Отправитель',
          'recipient_id'=>'Получатель',
          'summ'=>'Сумма перевода',
          'commision'=>'Комиссия',
        );
    }
}