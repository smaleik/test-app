<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cezar
 * Date: 01.10.14
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */

class AccountModel  extends MY_Model {
    public $id;
    public $client;
    public $serial;
    public $balance;

    /**
     * Название таблицы с которой работает модель
     * @return string
     */
    public function tableName(){
        return 'account';
    }

    /**
     * Правила для аттрибутов модели
     * @return array
     */
    public function rules(){
       return array(
            'client'=>'required',
            'serial'=>'required|is_unique[account.serial]',
            'balance'=>'required'
        );
    }

    /**
     * Подписи для атрибутов модели
     * @return array
     */
    public function attributeLabel(){
        return array(
          'client'=>'Клиент',
          'serial'=>'Номер счета',
          'balance'=>'Баланс'
        );
    }

    /**
     * Получить массив аккаунтов
     * @return array
     */
    public function getAll(){
        $data = $this->findAll();

        $result = array();

        foreach ($data as $account) {

            $result[$account->serial] = $account->client.":".$account->serial;

        }

        return $result;

    }
}