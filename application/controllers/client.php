<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('accountModel');
        $this->load->library('table');
    }

    protected function _render($view, $data = null){
        $this->load->view('/layouts/header');
        $this->load->view($view,$data);
        $this->load->view('/layouts/footer');
    }

    public function admin(){

        $tmpl = array('table_open'  => '<table class="table">');

        $this->table->set_template($tmpl);
        $this->table->set_heading('id','Клиент','Номер счета','Баланс');
        $this->table->function = 'htmlspecialchars';

        $accounts = $this->accountModel->activeDataProvider();
        $this->_render('/client/admin',array('accounts'=>$accounts));
    }

    public function create(){
        $account = new $this->accountModel;
        if ($account->validate())
        {
            $account->setAttributes($this->input->post(null,true));
            $account->save();
            redirect('/client/admin');
        } else {
            $this->_render('/client/create');
        }
    }

    public function update($id) {
        $account = $this->loadModel($id);

        if ($account->validate())
        {
            $account->setAttributes($this->input->post(null,true));
            $account->save();
            redirect('/client/admin');
        } else {
            $this->_render('/client/create');
        }
    }

    public function delete($id){
        $model = $this->loadModel($id);
        $model->delete();
        redirect('/client/admin');
    }

    public function detail($id){
        $this->load->model('accountDetailModel');

        $accountDetails = $this->accountDetailModel->findAllByAttributes(array('serial'=>$id));

        $this->_render('/client/detail',array('accountDetails'=>$accountDetails));

    }


    protected function loadModel($id){
        $model = $this->accountModel->findByPk($id);

        if  ($model===null) throw new Exception('Не найдена модель');

        return $model;
    }



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */