<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfer extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('transferModel');
        $this->load->library('table');
        $this->load->helper('form');

    }

    protected function _render($view, $data = null){
        $this->load->view('/layouts/header');
        $this->load->view($view,$data);
        $this->load->view('/layouts/footer');
    }

    public function admin(){

        $tmpl = array('table_open'  => '<table class="table">');

        $this->table->set_template($tmpl);

        $this->table->set_heading('id','Отправитель','Получатель','Сумма','Коммисия');
        $this->table->function = 'htmlspecialchars';

        $data = $this->transferModel->activeDataProvider();

        $this->_render('/transfer/admin',array('data'=>$data));
    }

    public function create(){
        $this->load->model('accountModel');
        $this->load->model('accountDetailModel');

        $transferModel = new $this->transferModel;
        if($this->input->post(null)){
           $transferModel->setAttributes($this->input->post(null,true));
        };

        if ($transferModel->validate())
        {
            // -- -- Начинаем транзакцию
            $this->db->trans_begin();

            $systemAccount = new $this->accountModel();
            $systemAccount = $systemAccount->findByAttributes(array('serial'=>TransferModel::SYSTEM_SERIAL));
            if ($systemAccount->id == null) {
                show_error("Не найден системный счет с номером ".TransferModel::SYSTEM_SERIAL);
            }

            $commission = $this->input->post('summ')*TransferModel::COMMISSION_STATE/100;

            $transferModel->commision = $commission;
            $systemAccount->balance=$systemAccount->balance + $commission;
            $systemAccount->save();

            $accountSender = new $this->accountModel;
            $accountSender = $accountSender->findByAttributes(array('serial'=>$transferModel->sender_id));
            $accountSender->balance = $accountSender->balance-$this->input->post('summ') - $commission;
            $accountSender->save();


            $accountDetailSender = new $this->accountDetailModel;
            $accountDetailSender->account_id = $accountSender->id;
            $accountDetailSender->serial = $accountSender->serial;
            $accountDetailSender->in = 0;
            $accountDetailSender->out = $this->input->post('summ') + $commission;
            $accountDetailSender->ostatok = $accountSender->balance;
            $accountDetailSender->save();

            $accountRecepient = new $this->accountModel;
            $accountRecepient = $accountRecepient->findByAttributes(array('serial'=>$transferModel->recipient_id));
            $accountRecepient->balance = $accountRecepient->balance + $this->input->post('summ');
            $accountRecepient->save();

            $accountDetailRecepient = new $this->accountDetailModel;
            $accountDetailRecepient->account_id = $accountRecepient->id;
            $accountDetailRecepient->serial = $accountRecepient->serial;
            $accountDetailRecepient->in = $this->input->post('summ');
            $accountDetailRecepient->out = 0;
            $accountDetailRecepient->ostatok = $accountRecepient->balance;
            $accountDetailRecepient->save();

            $transferModel->save();
            // Если без ошибок то применяем транзакцию
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            redirect('/transfer/admin');
        } else {
            $this->_render('/transfer/create');
        }
    }


    protected function loadModel($id){
        $model = $this->transferModel->findByPk($id);

        if  ($model===null) throw new Exception('Не найдена модель');

        return $model;
    }

    /**
     * Валидация счетов, они не должны совпадать
     * @param $recipient
     * @param $sender
     * @return bool
     */
    public function check_sender_recipient($recipient,$sender) {

        if ($recipient == $sender)
        {
            $this->form_validation->set_message('check_sender_recipient', 'Счет отправителя и получателя не должны совпадать');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * Валидация баланса, если боланс после отправки денег меньше нуля, то возвращается ошибка
     * @param $sender
     * @param $summ
     * @return bool
     */
    public function check_balance_sender($sender,$summ) {

        $this->load->model('accountModel');

        $account = new $this->accountModel;

        $account = $account->findByAttributes(array('serial'=>$sender));

        if ($account === null)
        {
            $this->form_validation->set_message('check_balance_sender', 'Не найден счет отправителя');
            return FALSE;
        }

        $balance = $account->balance - $summ-$summ*TransferModel::COMMISSION_STATE/100;

        if ($balance<0)
        {
            $this->form_validation->set_message('check_balance_sender', 'Недостаточно средств на счете отправителя');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */