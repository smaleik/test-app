<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cezar
 * Date: 01.10.14
 * Time: 14:19
 * To change this template use File | Settings | File Templates.
 */
class MY_Model extends CI_Model {

    private  $is_NewRecord = true;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Правила для аттрибутов модели
     * @return array
     */
    public function rules(){}

    /**
     * Подписи для атрибутов модели
     * @return array
     */
    public function attributeLabel(){}

    /**
     * Метод для валидации входных параметров для модели
     * @return bool
     */
    public function validate(){
        $this->load->library('form_validation');

        foreach ($this->rules() as $attr=>$rule) {
            $this->form_validation->set_rules($attr, $this->attributeLabel()[$attr], $rule);
        }
        return $this->form_validation->run();
    }


    /**
     * Получаем провайдер модели
     * @return object
     */
    public function activeDataProvider(){
        $ar = $this->db; /** @var  CI_DB_active_record $ar*/
        return $ar->get($this->tableName());
    }

    /**
     * Получить список всех моделей
     * @return mixed
     */
    public function findAll(){
        return $this->activeDataProvider()->result();
    }

    public function findByAttributes($params){
        $ar = $this->db; /** @var  CI_DB_active_record $ar*/
        $result = $ar->get_where($this->tableName(),$params)->result();

        if(count($result) == 0) {
            return null;
        }

        if ($result!==null) {
            $property = get_object_vars($this);

            foreach ($property as $attr=>$value) {
                if (property_exists($result[0],$attr)) {
                    $this->$attr = $result[0]->$attr;
                }
            }
            $this->is_NewRecord = false;

            return $this;
        }
    }

    public function findAllByAttributes($params){
        $ar = $this->db; /** @var  CI_DB_active_record $ar*/
        $result = $ar->get_where($this->tableName(),$params)->result();

        if(count($result) == 0) {
            return null;
        }

        if ($result!==null) {

            $property = get_object_vars($this);

            foreach ($result as $item) {
                $class = get_class($this);
                $model = new $class;
                $model->is_NewRecord = false;
                foreach ($property as $attr=>$value) {
                    if (property_exists($item,$attr)) {
                        $model->$attr = $item->$attr;
                    }
                }
                $obj[]=$model;
            }
            return $result;
        }
    }

    /**
     * Получить модель по id
     * @param $id
     * @return mixed
     */
    public function findByPk($id){
        $ar = $this->db; /** @var  CI_DB_active_record $ar*/
        $query = $ar->get_where($this->tableName(),array('id'=>$id));
        return $query->result();
    }

    public function save(){
        $ar = $this->db; /** @var  CI_DB_active_record $ar*/
        if ($this->is_NewRecord) {
            return $ar->insert($this->tableName(),$this);
        } else {
            return $this->updateByPk($this->id);
        }

    }

    public function updateByPk($id){
        return $this->db->update($this->tableName(), $this, array('id' => $id));
    }

    public function deleteByPk($id){
        $ar = $this->db; /** @var  CI_DB_active_record $ar*/
        return $ar->delete($this->tableName(), array('id' => $id));
    }

    public function delete(){
        return $this->db->delete($this->tableName(), array('id' => $this->id));
    }

    public function setAttributes($data){
        foreach ($data as $attr=>$value) {
            if(property_exists($this,$attr)) {
                $this->$attr = $value;
            }
        }
    }
}